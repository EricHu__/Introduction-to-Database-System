-- ------------------------
-- Populate Student table
-- ------------------------
INSERT INTO Student(Sno, Sname, Ssex, Sage, Sdept)
VALUES('201215121', '李勇', '男', 20, 'CS');
INSERT INTO Student(Sno, Sname, Ssex, Sage, Sdept)
VALUES('201215122', '刘晨', '女', 19, 'CS');
INSERT INTO Student(Sno, Sname, Ssex, Sage, Sdept)
VALUES('201215123', '王敏', '女', 18, 'MA');
INSERT INTO Student(Sno, Sname, Ssex, Sage, Sdept)
VALUES('201215125', '张立', '男', 19, 'IS');

-- ------------------------
-- Populate Course table
-- ------------------------
INSERT INTO Course(Cno, Cname, Cpno, Ccredit)
VALUES('6', '数据处理', NULL, 2);
INSERT INTO Course(Cno, Cname, Cpno, Ccredit)
VALUES('2', '数学', NULL, 2);
INSERT INTO Course(Cno, Cname, Cpno, Ccredit)
VALUES('7', 'PASCAL语言', '6', 4);
INSERT INTO Course(Cno, Cname, Cpno, Ccredit)
VALUES('4', '操作系统', '6', 3);
INSERT INTO Course(Cno, Cname, Cpno, Ccredit)
VALUES('5', '数据结构', '7', 4);
INSERT INTO Course(Cno, Cname, Cpno, Ccredit)
VALUES('1', '数据库', '5', 4);
INSERT INTO Course(Cno, Cname, Cpno, Ccredit)
VALUES('3', '信息系统', '1', 4);


-- ------------------------
-- Populate SC table
-- ------------------------
INSERT INTO SC(Sno, Cno, Grade)
VALUES('201215121', '1', 92);
INSERT INTO SC(Sno, Cno, Grade)
VALUES('201215121', '2', 85);
INSERT INTO SC(Sno, Cno, Grade)
VALUES('201215121', '3', 88);
INSERT INTO SC(Sno, Cno, Grade)
VALUES('201215122', '2', 90);
INSERT INTO SC(Sno, Cno, Grade)
VALUES('201215122', '3', 80);




